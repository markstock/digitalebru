# digital ebru #

## Description
Digital Ebru is a fast and diffusion-free interactive 2D fluid simulation code. It is implemented purely in compute shaders and using shared memory, so it runs about as fast as it computationally possible. It also uses novel algorithms developed over two decades of my own research. It contains what may be the first implementation of a multigrid Poisson solver in GLSL shaders.

Just run, click, and drag.

This wouldn't be possible without the support of scientists and programmers who came before me. Notably, this repository is a fork of [OpenGL with Luajit](https://bitbucket.org/jimbo00000/opengl-with-luajit), which is a simple testbed to get started in OpenGL programming using luajit. Binaries are included for Windows, Linux, and Solaris. It should work on all major platforms that have OpenGL drivers.

![digitalebru screenshot](data/digitalebru_cap2.png)

## Usage

```
./run.sh
```

[GLFW](http://www.glfw.org/) will handle window and context creation and input.

Some platforms require a bit of encouragement:

#### Windows
- [Download and run vcredist](https://www.microsoft.com/en-us/download/details.aspx?id=30679) for Visual Studio 11(2012).
- `./run.cmd`

#### Mac OSX
   MacOS support for OpenGL lags far behind the standard and cannot support compute shaders, the foundation of this code. I'm sorry.

#### Red Hat/Fedora
- `sudo dnf install SDL2 SDL2-devel glfw`
- `./run.sh`

#### TrueOS aka PCBSD aka FreeBSD
- Install GLFW from source with `BUILD_SHARED_LIBS=1`
- `sudo pkg install -fy luajit glfw`
- `sh`
- `LD_PRELOAD=/lib/libthr.so.3 luajit main_glfw.lua`  [Found here](https://bugs.pcbsd.org/issues/11079)

#### Solaris 11
- `sudo pkg install developer/versioning/git`
- `sudo pkg install gcc`
- `sudo pkgadd -d http://get.opencsw.org/now`
- `sudo /opt/csw/bin/pkgutil -U`
- `sudo /opt/csw/bin/pkgutil -y -i cmake`
- Build and install glfw with cmake 3.3.1 from `/opt/csw/bin/cmake`


## Acknowledgments

 - This repo is a direct fork of jimbo00000's excellent [OpenGL with Luajit](https://bitbucket.org/jimbo00000/opengl-with-luajit). See that repo for acknowledgments and influences.

