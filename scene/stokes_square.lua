--
-- stokes_square.lua
--
-- Run a 2D texture advection system with Stokeslet and source/sink forces
-- Advect using a 2nd order semi-Lagrangian method with high-order interpolation
--
-- (c)2019-20 Mark J. Stock markjstock@gmail.com
-- portions from compute_image.lua (c)2018 James Sussino
--

stokes_square = {}
stokes_square.__index = stokes_square

function stokes_square.new(...)
    local self = setmetatable({}, stokes_square)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")
local glFloatv = ffi.typeof('GLfloat[?]')

function stokes_square:init()
    self.vao = 0
    self.vbos = {}
    self.prog_draw = 0

    -- time step and diffusion coefficient (not used)
    self.dt = 0.1
    --self.nu = 0.0001
    self.pause = false

    -- size of work group in compute shaders
    self.gridx = 16
    self.gridy = 16

    -- base resolution (set the power to 1..6)
    -- levels=1 means run a sim on a texture with resolution gridx*2^1 cells
    -- gridx=16 and levels=4 means run a 256x256 sim, 6 is 1024^2, 8 is 4096^2
    self.levels = 6
    local xres = self.gridx * math.pow(2,self.levels)

    -- at lowest resolution are the streamfunction and associated velocity
    self.field = {}
    self.aspect = 1
    local yres = xres / self.aspect

    self.field["vel"] = {x=xres, y=yres, efmt=GL.GL_RG, ifmt=GL.GL_RG32F, mag=200}

    -- potentially at higher resolution is the conserved, drawn color field
    local color_mult = 2
    -- self.colsim is initial color field
    --   0 is checkerboard
    --   1 is random noise
    --   2 is one thin black line
    --   3 is vertical color bands
    --   4 is vertical black and white bands
    --   6 is horizontal black and white bands
    self.colsim = 3
    self.field["color"] = {x=xres*color_mult, y=yres*color_mult, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA32F}
    -- need a temporary to allow ping-ponging buffers
    self.field["tcol"] = {x=xres*color_mult, y=yres*color_mult, efmt=GL.GL_RGBA, ifmt=GL.GL_RGBA32F}

    -- self.drawmode is
    --   0 is drag (Stokes)
    --   1 is source
    --   2 is sink
    self.drawmode = 0

    self.mousing = false
    self.nmousept = 0
    self.maxmpts = 64
    self.mousevec = ffi.new("float[?]", 2*self.maxmpts)

    -- view point center and zoom
    self.vpx = 0.5
    self.vpy = 0.5
    self.vpz = 1.0

    self.frameno = 0
end

function stokes_square:setDataDirectory(dir)
    self.data_dir = dir
end

local basic_vert = [[
#version 310 es

in vec4 vPosition;
in vec4 vColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

out vec3 vfColor;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

-- render an rgba fragment
local basic_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    fragColor = vec4(texMag*col.xyz, 1.);
}
]]

-- render a single-channel (red) fragment as greyscale
local red_frag = [[
#version 310 es

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

uniform sampler2D sTex;
uniform float texMag;

void main()
{
    vec2 tc = vfColor.xy;
    tc.y = 1.-tc.y;
    vec4 col = texture(sTex, tc);
    float px = 0.5 + texMag * col.x;
    fragColor = vec4(px, px, px, 1.);
}
]]

local img_init_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(binding = 0) uniform sampler2D tex_in;
layout(rgba32f, binding = 1) uniform restrict image2D img_output;

uniform int uFillType;

float hash( float n ) { return fract(sin(n)*43758.5453); }

float hash(vec2 p) {
  return fract(sin(dot(p, vec2(43.232, 75.876)))*4526.3257);
}

void main() {
    vec4 pixel = vec4(0.0);
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    if (uFillType == 0)
    {
        // Init image to checkerboard
        if (((pixel_coords.x & 64) == 0)
         != ((pixel_coords.y & 64) == 0))
        {
            pixel = vec4(1.);
        }
    }
    else if (uFillType == 1)
    {
        // Random init
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            float(pixel_coords.y)/float(imageSize(img_output).y));
        float hval = hash(coord);
        pixel = hval > .5 ? vec4(1.) : vec4(0.);
    }
    else if (uFillType == 2)
    {
        // Single black pixel in center init
        pixel = vec4(1.);
        if (pixel_coords.x == imageSize(img_output).x/2)
            pixel = vec4(0.);
    }
    else if (uFillType == 3)
    {
        // Random bands of color
        float xc = floor(pixel_coords.x / 64.0);
        vec2 coord = vec2(xc, 0.0);
        pixel = vec4(0.1+0.7*hash(vec2(xc, 0.0)),
                     0.2+0.7*hash(vec2(xc, 1.0)),
                     0.3+0.7*hash(vec2(xc, 2.0)),
                     1.0);
    }
    else if (uFillType == 4)
    {
        // Vertical bands of black and white
        if ((pixel_coords.x & 128) == 0)
        {
            pixel = vec4(1.);
        }
    }
    else if (uFillType == 5)
    {
        // sample from the input texture
        vec2 coord = vec2(
            float(pixel_coords.x)/float(imageSize(img_output).x),
            1.0-float(pixel_coords.y)/float(imageSize(img_output).y));
        vec4 currpix = imageLoad(img_output, pixel_coords);
        vec4 newpixl = texture(tex_in, coord);
        float a = newpixl.w;
        pixel = vec4((1.0-a)*currpix.xyz + a*newpixl.xyz, 1.0);
    }
    else if (uFillType == 6)
    {
        // Horizontal bands of black and white
        if ((pixel_coords.y & 128) == 0)
        {
            pixel = vec4(1.);
        }
    }

    imageStore(img_output, pixel_coords, pixel);
}
]]

-- modify a field of color
local img_mod_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(rgba32f, binding = 0) uniform restrict image2D img_inout;

uniform float brite_in;
uniform float contrast_in;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    vec4 pixel = imageLoad(img_inout, pixel_coords);

    pixel = (1.0+brite_in) * pixel;
    pixel = vec4(0.5) + (1.0+contrast_in) * (pixel-vec4(0.5));

    imageStore(img_inout, pixel_coords, pixel);
}
]]

-- generate a velocity field from a single Stokeslet
local img_stokeslet_to_vel = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(rg32f, binding = 0) uniform writeonly restrict image2D img_vel_out;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // a source and force direction
    vec2 srcpt = vec2(0.5, 0.5);
    vec2 force = vec2(1.0, 0.0);

    // this is 1/8pimu, the constant
    const float fac = 1.0f / imageSize(img_vel_out).x;

    // find distance from this pixel to the source pixel
    vec2 coord = vec2(float(pixel_coords.x)/float(imageSize(img_vel_out).x),
                  1.0-float(pixel_coords.y)/float(imageSize(img_vel_out).y) );

    vec2 dx = coord - srcpt;

    float distsq = dot(dx,dx) + 0.0001f;
    float dist = sqrt(distsq);
    float coeff = fac / (distsq*dist);

    // push stuff away from the source point
    float u = coeff * ( force.x*(distsq+dx.x*dx.x) + force.y*dx.x*dx.y );
    float v = coeff * ( force.y*(distsq+dx.y*dx.y) + force.x*dx.x*dx.y );

    // force no flow through the boundaries
    u *= coord.x * (1.f-coord.x);
    v *= coord.y * (1.f-coord.y);

    // with this line, 4 loads, 2 stores, 5 flops, 8 iops
    imageStore(img_vel_out, pixel_coords, vec4(u, v, 0, 1));
}
]]

-- take the list of source points/lines and use Stokes flow to generate a velocity field
local img_drag_to_vel = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(rg32f, binding = 0) uniform writeonly restrict image2D img_vel_out;

uniform int nsegs;
uniform float xseg[128];

void main() {

    // float coords of this pixel
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    vec2 coord = vec2(float(pixel_coords.x)/float(imageSize(img_vel_out).x),
                      float(pixel_coords.y)/float(imageSize(img_vel_out).y) );

    // this is 1/8pimu, the constant
    const float fac = 16.0f / imageSize(img_vel_out).x;

    // prepare the summation
    vec2 vel = vec2(0.0f, 0.0f);

    // loop over segments
    for (int i=0; i<nsegs; ++i) {

        // these numbers are already square and normalized [0..1]
        const vec2 x1 = vec2(xseg[2*i+0], xseg[2*i+1]);
        const vec2 x2 = vec2(xseg[2*i+2], xseg[2*i+3]);
        const vec2 dseg = x2 - x1;

        // find distance of the line segment
        const float l2 = pow(dseg.x, 2) + pow(dseg.y, 2);

        // as long as segment is not zero-length
        if (l2 > 0.0) {

            // find length
            const float l1 = sqrt(l2);

            // scale the force - this is dramatic
            //vec2 force = dseg;

            // this is very stable
            //vec2 force = 0.004 * dseg / l1;

            // this is somewhere in between
            vec2 force = 0.07 * dseg / sqrt(l1);

            // break line up into pixel-sized fragments
            const int npts = 1 + int(l1 * float(imageSize(img_vel_out).x));
            const float oonpts = 1.f / float(npts);

            // for each point along this line
            for (int j=0; j<npts; ++j) {

                // a source and force direction
                vec2 srcpt = x1 + dseg*float(j)*oonpts;

                // find distance from this pixel to the source pixel
                vec2 dx = coord - srcpt;

                // the constant provides a desingularization radius of about 0.01
                float distsq = dot(dx,dx) + 0.0001f;
                float dist = sqrt(distsq);
                float coeff = fac / (distsq*dist);

                // push stuff away from the source point
                vel.x += coeff * ( force.x*(distsq+dx.x*dx.x) + force.y*dx.x*dx.y );
                vel.y += coeff * ( force.y*(distsq+dx.y*dx.y) + force.x*dx.x*dx.y );

                // force no flow through the boundaries
                // u *= coord.x * (1.f-coord.x);
                // v *= coord.y * (1.f-coord.y);
            }
        }
    }

    // with this line, 4 loads, 2 stores, 5 flops, 8 iops
    imageStore(img_vel_out, pixel_coords, vec4(vel.x, vel.y, 0, 1));
}
]]

-- take the list of points/lines and use source/sinks to generate a velocity field
local img_source_to_vel = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(rg32f, binding = 0) uniform writeonly restrict image2D img_vel_out;

uniform int nsegs;
uniform float xseg[128];

void main() {

    // float coords of this pixel
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    vec2 coord = vec2(float(pixel_coords.x)/float(imageSize(img_vel_out).x),
                      float(pixel_coords.y)/float(imageSize(img_vel_out).y) );

    // this is the constant
    const float fac = 0.001f / imageSize(img_vel_out).x;
    //const float fac = 0.00002f / imageSize(img_vel_out).x;

    // prepare the summation
    vec2 vel = vec2(0.0f, 0.0f);

    // loop over segments
    for (int i=0; i<nsegs; ++i) {

        // these numbers are already square and normalized [0..1]
        const vec2 x1 = vec2(xseg[2*i+0], xseg[2*i+1]);
        const vec2 x2 = vec2(xseg[2*i+2], xseg[2*i+3]);
        const vec2 dseg = x2 - x1;

        // find distance of the line segment
        const float l2 = pow(dseg.x, 2) + pow(dseg.y, 2);

        // as long as segment is not zero-length
        if (l2 > 0.0) {

            // find length
            const float l1 = sqrt(l2);

            // break line up into pixel-sized fragments
            const int npts = 1 + int(l1 * float(imageSize(img_vel_out).x));
            const float oonpts = 1.f / float(npts);

            // for each point along this line
            for (int j=0; j<npts; ++j) {

                // a source location
                vec2 srcpt = x1 + dseg*float(j)*oonpts;

                // find distance from this pixel to the source pixel
                vec2 dx = coord - srcpt;

                // the constant provides a desingularization radius of about 0.01
                float distsq = dot(dx,dx) + 0.0001f;
                float coeff = fac / (l1*distsq);
                //float coeff = fac / (l1*distsq*sqrt(distsq));

                // push stuff away from the source point
                //vel.x += coeff * dx.x;
                //vel.y += coeff * dx.y;

                // push, but force no flow through the boundaries
                vel.x += 2.0f * coeff * dx.x * coord.x * (1.f-coord.x);
                vel.y += 2.0f * coeff * dx.y * coord.y * (1.f-coord.y);
            }
        }
    }

    // with this line, 4 loads, 2 stores, 5 flops, 8 iops
    imageStore(img_vel_out, pixel_coords, vec4(vel.x, vel.y, 0, 1));
}
]]

-- semi-lagrangian advection for rgba32f, 2nd order in time, 1st order in space
local comp_sla_rk2_lin_blocked_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(rgba32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);
    vec4 pixel = texture(tex_scalar_in, tc2);

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);
}
]]

-- semi-lagrangian advection for rgba32f, 2nd order in time, ? order in space
local comp_sla_rk2_m4h_blocked_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(binding = 1) uniform sampler2D tex_scalar_in;
layout(rgba32f, binding = 2) uniform image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the scalar there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);

    // now use a hack of M4' to pull the color
    float dx = 1.0 / float(imageSize(img_scalar_out).x);
    vec4 pixel = 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, -1.0))
               + 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, -1.0))
               + 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, +1.0))
               + 0.015625 * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, +1.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, +1.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, -1.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, +0.0))
               + -0.15625 * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, +0.0))
               +   1.5625 * texture(tex_scalar_in, tc2) ;

    //float dc = 0.01;
    //vec4 pixel = (1.0+4.0*dc) * texture(tex_scalar_in, tc2)
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, +1.0))
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(+0.0, -1.0))
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(+1.0, +0.0))
    //           - dc * texture(tex_scalar_in, tc2 + dx*vec2(-1.0, +0.0)) ;

    pixel = clamp(pixel, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);
}
]]

-- semi-lagrangian advection for rgba32f, 2nd order in time, 4th order in space
local comp_sla_rk2_m4p_blocked_rgba32f = [[
#version 430
layout(local_size_x = GSX, local_size_y = GSY) in;
layout(binding = 0) uniform sampler2D tex_vel_in;
layout(rgba32f, binding = 1) uniform readonly restrict image2D img_scalar_in;
layout(rgba32f, binding = 2) uniform writeonly restrict image2D img_scalar_out;

uniform float dt;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    // sample velocity at the current location
    vec2 tc = vec2( float(0.5+pixel_coords.x) / float(imageSize(img_scalar_out).x),
                    float(0.5+pixel_coords.y) / float(imageSize(img_scalar_out).y) );
    vec4 vel = texture(tex_vel_in, tc);

    // march backwards in time and sample the velocity there
    vec2 tc1 = tc - dt*vel.xy;
    vec4 vel1 = texture(tex_vel_in, tc1);

    // combine the two to find the true velocity
    vec2 tc2 = tc - 0.5*dt*(vel.xy+vel1.xy);

    // now use real M4' to pull the color
    vec2 xc = tc2 * float(imageSize(img_scalar_in).x) - vec2(0.5);
    ivec2 ic = ivec2(floor(xc));
    vec2 dx = xc - vec2(ic);

    // the M4' weights
    vec4 xwt = vec4( 0.5*pow(1-dx.x,2)*(-dx.x),
                     1.0 - dx.x*dx.x*(2.5 - 1.5*dx.x),
                     1.0 - pow(1-dx.x,2)*(1.0 + 1.5*dx.x),
                     0.5*dx.x*dx.x*(dx.x-1) );
    vec4 ywt = vec4( 0.5*pow(1-dx.y,2)*(-dx.y),
                     1.0 - dx.y*dx.y*(2.5 - 1.5*dx.y),
                     1.0 - pow(1-dx.y,2)*(1.0 + 1.5*dx.y),
                     0.5*dx.y*dx.y*(dx.y-1) );

    // the indices
    ivec4 ix = ivec4(mod( vec4(ic.x-1, ic.x, ic.x+1, ic.x+2), vec4(imageSize(img_scalar_in).x) ));
    ivec4 iy = ivec4(mod( vec4(ic.y-1, ic.y, ic.y+1, ic.y+2), vec4(imageSize(img_scalar_in).y) ));

    // up to here: 136 flops and 4 loads

    // this is 64 loads and 140 flops
    vec4 pixel = xwt.x * ywt.x * imageLoad(img_scalar_in, ivec2(ix.x, iy.x))
               + xwt.y * ywt.x * imageLoad(img_scalar_in, ivec2(ix.y, iy.x))
               + xwt.z * ywt.x * imageLoad(img_scalar_in, ivec2(ix.z, iy.x))
               + xwt.w * ywt.x * imageLoad(img_scalar_in, ivec2(ix.w, iy.x))
               + xwt.x * ywt.y * imageLoad(img_scalar_in, ivec2(ix.x, iy.y))
               + xwt.y * ywt.y * imageLoad(img_scalar_in, ivec2(ix.y, iy.y))
               + xwt.z * ywt.y * imageLoad(img_scalar_in, ivec2(ix.z, iy.y))
               + xwt.w * ywt.y * imageLoad(img_scalar_in, ivec2(ix.w, iy.y))
               + xwt.x * ywt.z * imageLoad(img_scalar_in, ivec2(ix.x, iy.z))
               + xwt.y * ywt.z * imageLoad(img_scalar_in, ivec2(ix.y, iy.z))
               + xwt.z * ywt.z * imageLoad(img_scalar_in, ivec2(ix.z, iy.z))
               + xwt.w * ywt.z * imageLoad(img_scalar_in, ivec2(ix.w, iy.z))
               + xwt.x * ywt.w * imageLoad(img_scalar_in, ivec2(ix.x, iy.w))
               + xwt.y * ywt.w * imageLoad(img_scalar_in, ivec2(ix.y, iy.w))
               + xwt.z * ywt.w * imageLoad(img_scalar_in, ivec2(ix.z, iy.w))
               + xwt.w * ywt.w * imageLoad(img_scalar_in, ivec2(ix.w, iy.w))
    ;

    // 8 more flops
    pixel = clamp(pixel, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));

    // store the new scalar value in the output buffer
    imageStore(img_scalar_out, pixel_coords, pixel);

    // total is 68 loads, 4 stores, 284 flops
}
]]


function stokes_square:initTriAttributes()
    local glIntv = ffi.typeof('GLint[?]')
    local glUintv = ffi.typeof('GLuint[?]')

    local verts = glFloatv(4*3, {
        0,0,0,
        1,0,0,
        1,1,0,
        0,1,0,
    })

    local vpos_loc = gl.glGetAttribLocation(self.prog_draw, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog_draw, "vColor")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    local cvbo = glIntv(0)
    gl.glGenBuffers(1, cvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, cvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vcol_loc, 3, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, cvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
end

function stokes_square:initTextureImage(w, h, ifmt, dtyp, efmt)
    gl.glActiveTexture(GL.GL_TEXTURE0)
    local dtxId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, dtxId)
    local texID = dtxId[0]
    gl.glBindTexture(GL.GL_TEXTURE_2D, texID)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_REPEAT)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
    gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAX_LEVEL, 0)
    gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, ifmt,
                    w, h, 0,
                    efmt, dtyp, nil)
    gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    gl.glBindImageTexture(0, texID, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, ifmt)

    return texID
end

function stokes_square:initializeTextures()
    for k,v in pairs(self.field) do
        print("stokes_square.initializeTextures initializing (",k,")")

        -- generate and set the tid
        local texID = self:initTextureImage(v.x, v.y, v.ifmt, GL.GL_FLOAT, v.efmt)
        v.tid = texID

        -- set the initialization program
        if k == "color" then v.init_prog = self.prog_init_rgba32f end
    end
end

function stokes_square:adjustColor(brite, contrast)
    local v = self.field.color

    gl.glBindImageTexture(0, v.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_WRITE, v.ifmt)
    local thisprog = self.prog_mod_rgba32f
    gl.glUseProgram(thisprog)

    local brite_loc = gl.glGetUniformLocation(thisprog, "brite_in")
    gl.glUniform1f(brite_loc, brite)
    local contr_loc = gl.glGetUniformLocation(thisprog, "contrast_in")
    gl.glUniform1f(contr_loc, contrast)

    gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridy, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
end

function stokes_square:loadTextureIntoColor(overlayTexId)
    --print("loadTextureIntoColor taking tid ",overlayTexId," over color")
    local v = self.field.color

    gl.glBindImageTexture(1, v.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, v.ifmt)
    gl.glUseProgram(v.init_prog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, overlayTexId)
    local tex_loc0 = gl.glGetUniformLocation(v.init_prog, "tex_in")
    gl.glUniform1i(tex_loc0, 0)

    local sfill_loc = gl.glGetUniformLocation(v.init_prog, "uFillType")
    --gl.glUniform1i(sfill_loc, 0)
    gl.glUniform1i(sfill_loc, 5)

    gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridy, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
end

function stokes_square:clearTextures(condition, overlayTexId)
    for k,v in pairs(self.field) do
        if v.init_prog ~= nil then
            print("clearTextures resetting (",k,") tid is",v.tid)

            local fill_type = 0
            if k == "color" then fill_type = self.colsim end

            gl.glBindImageTexture(1, v.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, v.ifmt)
            gl.glUseProgram(v.init_prog)

            -- if a texture ID is given in the args, overlay that
            if overlayTexId ~= nil then
                gl.glActiveTexture(GL.GL_TEXTURE0)
                gl.glBindTexture(GL.GL_TEXTURE_2D, overlayTexId)
                local tex_loc0 = gl.glGetUniformLocation(v.init_prog, "tex_in")
                gl.glUniform1i(tex_loc0, 0)
                -- make sure to override the fill type
                fill_type = 5
            end

            local sfill_loc = gl.glGetUniformLocation(v.init_prog, "uFillType")
            gl.glUniform1i(sfill_loc, fill_type)

            gl.glDispatchCompute(v.x/self.gridx, v.y/self.gridy, 1)
            gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        end
    end
end

function stokes_square:clearSolution()
end

function stokes_square:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    -- two draw programs, one for rg and rgb buffers, another for just r
    self.prog_draw = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    self.prog_scalar = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = red_frag,
        })

    -- init programs for setting and resetting the fields
    self.prog_init_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(img_init_rgba32f, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    self.prog_mod_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(img_mod_rgba32f, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    -- and our solver programs
    self.prog_stokeslet_to_vel = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(img_stokeslet_to_vel, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    self.prog_drag_to_vel = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(img_drag_to_vel, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    self.prog_source_to_vel = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(img_source_to_vel, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    -- and advection programs
    self.prog_sladvect_2_blocked_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(comp_sla_rk2_lin_blocked_rgba32f, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    self.prog_sladvect_3_blocked_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(comp_sla_rk2_m4h_blocked_rgba32f, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    self.prog_sladvect_4_blocked_rgba32f = sf.make_shader_from_source({
        compsrc = string.gsub(string.gsub(comp_sla_rk2_m4p_blocked_rgba32f, "GSX", self.gridx, 1), "GSY", self.gridx, 1),
        })

    -- init the meshes and textures
    self:initTriAttributes()
    self:initializeTextures(0)
    self:clearTextures(0)

    gl.glBindVertexArray(0)
end

function stokes_square:advectScalars(dt)
    -- we are only advecting the color texture
    --local thisprog = self.prog_sladvect_2_blocked_rgba32f
    --local thisprog = self.prog_sladvect_3_blocked_rgba32f
    local thisprog = self.prog_sladvect_4_blocked_rgba32f
    gl.glUseProgram(thisprog)

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    local tex_loc0 = gl.glGetUniformLocation(thisprog, "tex_vel_in")
    gl.glUniform1i(tex_loc0, 0)

    -- load the old color as an image
    gl.glBindImageTexture(1, self.field.color.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_ONLY, self.field.color.ifmt)
    gl.glBindImageTexture(2, self.field.tcol.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.tcol.ifmt)

    local dt_loc = gl.glGetUniformLocation(thisprog, "dt")
    gl.glUniform1f(dt_loc, dt)

    gl.glDispatchCompute(self.field.color.x/self.gridx, self.field.color.y/self.gridy, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- now swap the buffers
    local tempid = self.field.color.tid
    self.field.color.tid = self.field.tcol.tid
    self.field.tcol.tid = tempid
end

-- given source point(s) or stokeslet, generate velocities
function stokes_square:singleStokeslet()

    gl.glBindImageTexture(0, self.field.vel.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.vel.ifmt)

    -- test with one stokeslet, or a drawn line
    gl.glUseProgram(self.prog_stokeslet_to_vel)

    -- call it here
    gl.glDispatchCompute(self.field.vel.x/self.gridx, self.field.vel.y/self.gridy, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    --gl.glUseProgram(0)
end

-- use the self.mousevec to generate this frames path
function stokes_square:mousePathToVel()
    --print("stokes_square.mousePathToVel processing ", self.nmousept, " points")
    local zerovels = false

    -- what we do depends on the current and previous number of points
    if self.mousing then
        -- we drew lines last step, so we have one point in the array
        if self.nmousept == 0 then
            -- should not happen
            self.mousing = false
            zerovels = true
        elseif self.nmousept == 1 then
            -- no new points, we stop drawing
            self.mousing = false
            self.nmousept = 0
            zerovels = true
        else
            -- there are new points, draw n-1 segments
        end

    else
        -- we did not draw last step
        if self.nmousept == 0 then
            -- no points, still not drawing
            zerovels = true
        elseif self.nmousept == 1 then
            -- one new point, save it, and try to draw next step
            self.mousing = true
            zerovels = true
        else
            -- more than one new point, draw n-1 segments
            self.mousing = true
        end
    end

    -- if we are here, we are going to draw n-1 segments
    --print("stokes_square.mousePathToVel drawing ", (self.nmousept-1), " segments")

    -- call the shader to compute the new velocity
    local thisprog
    if self.drawmode == 0 then
      thisprog = self.prog_drag_to_vel
    elseif self.drawmode == 1 then
      thisprog = self.prog_source_to_vel
    else
      thisprog = self.prog_source_to_vel
    end
    gl.glUseProgram(thisprog)

    --gl.glActiveTexture(GL.GL_TEXTURE0)
    --gl.glBindTexture(GL.GL_TEXTURE_2D, self.field.vel.tid)
    --gl.glBindImageTexture(1, self.field.vort.tid, 0, GL.GL_FALSE, 0, GL.GL_READ_WRITE, self.field.vort.ifmt)
    gl.glBindImageTexture(0, self.field.vel.tid, 0, GL.GL_FALSE, 0, GL.GL_WRITE_ONLY, self.field.vel.ifmt)

    -- upload the mouse movement array and array size
    local ns_loc = gl.glGetUniformLocation(thisprog, "nsegs")
    local xm_loc = gl.glGetUniformLocation(thisprog, "xseg")
    if zerovels then
      gl.glUniform1i(ns_loc, 0)
      gl.glUniform1fv(xm_loc, 0, self.mousevec)
    else
      gl.glUniform1i(ns_loc, self.nmousept-1)
      gl.glUniform1fv(xm_loc, 2*self.nmousept, self.mousevec)
    end

    -- call the shader here
    gl.glDispatchCompute(self.field.vel.x/self.gridx, self.field.vel.y/self.gridy, 1)
    gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)

    -- copy the last mouse point to the first position and "remove" the rest
    if not zerovels then
        self.mousevec[0] = self.mousevec[2*self.nmousept-2]
        self.mousevec[1] = self.mousevec[2*self.nmousept-1]
        self.nmousept = 1
    end
end

function stokes_square:exitGL()
    gl.glBindVertexArray(self.vao)
    for _,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end
    self.vbos = {}
    -- delete programs
    gl.glDeleteProgram(self.prog_draw)
    gl.glDeleteProgram(self.prog_scalar)
    gl.glDeleteProgram(self.prog_init_rgba32f)
    gl.glDeleteProgram(self.prog_mod_rgba32f)
    gl.glDeleteProgram(self.prog_stokeslet_to_vel)
    gl.glDeleteProgram(self.prog_drag_to_vel)
    gl.glDeleteProgram(self.prog_source_to_vel)
    gl.glDeleteProgram(self.prog_sladvect_2_blocked_rgba32f)
    gl.glDeleteProgram(self.prog_sladvect_3_blocked_rgba32f)
    gl.glDeleteProgram(self.prog_sladvect_4_blocked_rgba32f)

    -- delete textures
    for _,v in pairs(self.field) do
        local tid = ffi.new("GLuint[1]", v.tid)
        gl.glDeleteTextures(1,tid)
    end
    -- clean up
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function stokes_square:render_one_texture(tex, prog, view, proj)

    gl.glUseProgram(prog)
    local umv_loc = gl.glGetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.glGetUniformLocation(prog, "prmtx")
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, tex.tid)
    local stex_loc = gl.glGetUniformLocation(prog, "sTex")
    gl.glUniform1i(stex_loc, 0)

    local to_loc = gl.glGetUniformLocation(prog, "texMag")
    local val_mag = 1.0
    if tex.mag ~= nil then val_mag = tex.mag end
    gl.glUniform1f(to_loc, val_mag)

    gl.glUniformMatrix4fv(umv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
end

function stokes_square:render_for_one_eye(view, proj)

    gl.glBindVertexArray(self.vao)

    local vp = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    local win_w,win_h = vp[2]-vp[0], vp[3]-vp[1]
    local aspect = win_w / win_h

    gl.glClearColor(0.0, 0.0, 0.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

    -- orthographic projection
    local newproj = {}
    mm.glh_ortho(newproj, -aspect, aspect, -1, 1, 0.1, 10.0)

    -- view matrix
    local newview = {}
    mm.make_identity_matrix(newview)
    local s = aspect
    s = aspect * 2.0
    if aspect < 1.0 then
        s = 2.0
    end
    s = s / self.vpz
    mm.glh_scale(newview, s, s, s)
    mm.glh_translate(newview, -self.vpx, -self.vpy, -1)

    -- draw only the color scalar
    self:render_one_texture(self.field.color, self.prog_draw, newview, newproj)

    gl.glBindVertexArray(0)
    gl.glUseProgram(0)
end

function stokes_square:readpixels()

    local vp = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)

    -- assure alignment of 4, png size needs to by dividable by 4
    --int width = (viewport[2]/4) * 4;
    --int height = (viewport[3]/4) * 4;
    --int nPixels = width * height;
    local nPixels = vp[2]*vp[3]

    -- GL_RGB implies "3" in two other places
    local buffer = ffi.new("GLubyte[?]", 3*nPixels)
    local bytes_ptr = ffi.cast('unsigned char*', buffer)

    gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 1)
    gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1)
    gl.glReadBuffer(GL.GL_BACK);
    --gl.glMemoryBarrier(GL.GL_FRAMEBUFFER_BARRIER_BIT)
    --gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
    --gl.glMemoryBarrier(GL.GL_TEXTURE_UPDATE_BARRIER_BIT)
    --gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)
    gl.glFinish()
    gl.glReadPixels(0, 0, vp[2], vp[3], GL.GL_RGB, GL.GL_UNSIGNED_BYTE, bytes_ptr)
    --gl.glReadPixels(0, 0, vp[2], vp[3], GL.GL_RGB, GL.GL_UNSIGNED_BYTE, buffer)
    --gl.glMemoryBarrier(GL.GL_FRAMEBUFFER_BARRIER_BIT)
    gl.glFinish()
    --gl.glMemoryBarrier(GL.GL_FRAMEBUFFER_BARRIER_BIT)
    --gl.glMemoryBarrier(GL.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
    --gl.glMemoryBarrier(GL.GL_TEXTURE_UPDATE_BARRIER_BIT)
    --gl.glMemoryBarrier(GL.GL_SHADER_STORAGE_BARRIER_BIT)

    --function bytes(x)
    --    local b4=x%256  x=(x-x%256)/256
    --    local b3=x%256  x=(x-x%256)/256
    --    local b2=x%256  x=(x-x%256)/256
    --    local b1=x%256  x=(x-x%256)/256
    --    return string.char(b4,b3,b2,b1)
    --end

    -- now write it
    local framename = "img"..string.format("%06d",self.frameno)..".bin"
    --print("write screenshot to", framename)
    local out = io.open(framename, 'wb')
    --out:write(bytes(vp[2]))
    --out:write(bytes(vp[3]))
    for i=0,3*nPixels-1 do
        out:write(string.char(bytes_ptr[i]))
    end
    out:flush()
    out:close()

    -- convert to png with
    -- convert -size "1920x1080" -depth 8 RGB:img000000.bin img000000.png

    -- increment counter for next time
    self.frameno = self.frameno + 1

end

function stokes_square:timestep(absTime, dt)
    if not self.pause then
        -- advect vorticity, scalar, and color
        self:advectScalars(self.dt)
        -- process mouse drag array to generate movement
        self:mousePathToVel()
        -- process fixed stokeslet to generate movement
        --self:singleStokeslet()
        --print("stokes_square.timestep", self.dt)
    end
end

function stokes_square:keypressed(ch, tid)

    -- this is because glfw turns all char presses to capital letters, but SDL does not
    if ch > 96 and ch < 123 then
        ch = ch - 32
    end

    if ch == string.byte(' ') then
        self.pause = not self.pause

    elseif ch == string.byte('=') then
        if tid ~= nil then
            self:loadTextureIntoColor(tid)
        end

    elseif ch == string.byte('[') then
        self.dt = self.dt / 1.2

    elseif ch == string.byte(']') then
        self.dt = self.dt * 1.2

    elseif ch == string.byte('W') then
        self.vpy = self.vpy + 0.01

    elseif ch == string.byte('S') then
        self.vpy = self.vpy - 0.01

    elseif ch == string.byte('A') then
        self.vpx = self.vpx - 0.01

    elseif ch == string.byte('D') then
        self.vpx = self.vpx + 0.01

    elseif ch == string.byte('Q') then
        -- zoom out
        self.vpz = self.vpz * 1.01

    elseif ch == string.byte('E') then
        -- zoom in
        self.vpz = self.vpz / 1.01

    elseif ch == string.byte('R') then
        -- reset view
        self.vpx = 0.5
        self.vpy = 0.5
        self.vpz = 1.0

    elseif ch == string.byte('I') then
        -- adjust brightness of color
        self:adjustColor(0.1, 0.0)

    elseif ch == string.byte('K') then
        -- adjust brightness of color
        self:adjustColor(-0.1, 0.0)

    elseif ch == string.byte('J') then
        -- adjust color contrast
        self:adjustColor(0.0, -0.1)

    elseif ch == string.byte('L') then
        -- adjust color contrast
        self:adjustColor(0.0, 0.1)

    elseif ch == string.byte('P') then
        -- dump frame to image (png or raw?)
        self:readpixels()

    end

end

function stokes_square:onmouse(x, y)
    --multiple of these can trigger in the span of a time step
    if (self.nmousept == self.maxmpts) then return end

    local vp = ffi.new("int[4]")
    gl.glGetIntegerv(GL.GL_VIEWPORT, vp)
    local win_w,win_h = vp[2]-vp[0], vp[3]-vp[1]
    local aspect = win_w / win_h

    --print("stokes_square.onmouse scaling is ", self.vpx, self.vpy, self.vpz)
    --print("stokes_square.onmouse", x, y, aspect)
    -- this should be done with an inverse transform of one of the projection matrices
    newx = x
    newy = y
    if aspect > 1.0 then
        newx = x + self.vpx - 0.5
        newy = 1.0 - self.vpy + (y-0.5)/aspect
    else
        newx = self.vpx + (x-0.5)*aspect
        newy = y - self.vpy + 0.5
    end
    --print("stokes_square.onmouse", x, y, " xformed", newx, newy)
    self.mousevec[self.nmousept*2+0] = newx
    self.mousevec[self.nmousept*2+1] = newy
    self.nmousept = self.nmousept + 1
end

function stokes_square:onSingleTouch(pointerid, action, x, y)
    --print("stokes_square.onSingleTouch",pointerid, action, x, y)
end

return stokes_square
