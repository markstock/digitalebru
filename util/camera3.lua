-- camera3.lua
-- Simple animated motion

local mm = require 'util.matrixmath'

camera3 = {}
camera3.__index = camera3

function camera3.new(...)
    local self = setmetatable({}, camera3)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function camera3:init()
    self.chassis = {0,1,1}
    self.rotx = 0
end

function camera3:reset()
    self:init()
end

function camera3:timestep(absTime, dt)
    self.rotx = math.sin(absTime)
end

function camera3:getViewMatrix()
    local v = {}
    mm.make_identity_matrix(v)
    mm.glh_translate(v, self.chassis[1], self.chassis[2], self.chassis[3])
    mm.glh_rotate(v, 45*self.rotx, 0,1,0)
    return v
end

return camera3
