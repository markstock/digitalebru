-- cmdprompt.lua
-- A buffer for typed commands

local ffi = require 'ffi'
local mm = require 'util.matrixmath'
local sf2 = require 'util.shaderfunctions2'

cmdprompt = {}
cmdprompt.__index = cmdprompt

function cmdprompt.new(...)
    local self = setmetatable({}, cmdprompt)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function cmdprompt:init()
    self.vao = 0
    self.vbos = {}
    self.time = 0

    self.glfont = nil
    self.win_w = 1000
    self.win_h = 800
    self.text_scale = .35

    self.cmd_buf = ''
    self.show = false
    self.ctrldown = false
    self.prompt_text = ''

    self.lines = {}
end

function cmdprompt:setDataDirectory(dir)
    self.data_dir = dir
end

local basic_vert = [[
#version 310 es
in vec4 vPosition;
void main() { gl_Position = vPosition; }
]]
local frag_body_backdrop = [[
#version 310 es
#ifdef GL_ES
precision mediump float;
#endif
out vec4 fragColor;
void main() { fragColor = vec4(0.,0.,0.,.75); }
]]

function cmdprompt:initBackdropAttributes()
    local glIntv   = ffi.typeof('GLint[?]')
    local glFloatv = ffi.typeof('GLfloat[?]')

    -- One quad not leaving a border around the screen edge
    local s = 0.9
    local verts = glFloatv(4*2, {
        -s,-.15, -- TODO: adjust size of backdrop to contents
        s,-.15,
        s, s,
        -s,s,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog_backdrop, "vPosition")

    local vvbo = glIntv(0)
    gl.glGenBuffers(1, vvbo)
    gl.glBindBuffer(GL.GL_ARRAY_BUFFER, vvbo[0])
    gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
    gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    table.insert(self.vbos, vvbo)

    gl.glEnableVertexAttribArray(vpos_loc)
end

function cmdprompt:initGL()
    local vaoId = ffi.new("int[1]")
    gl.glGenVertexArrays(1, vaoId)
    self.vao = vaoId[0]
    gl.glBindVertexArray(self.vao)

    -- Create an alpha 0.5 backdrop to turn down scene brightness for UI
    self.prog_backdrop = sf2.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = frag_body_backdrop,
        })
    self:initBackdropAttributes()

    gl.glBindVertexArray(0)

    local dir = "fonts"
    if self.data_dir then dir = self.data_dir .. "/" .. dir end
    self.glfont = GLFont.new('courier_512.fnt', 'courier_512_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function cmdprompt:exitGL()
    self.glfont:exitGL()
    gl.glBindVertexArray(self.vao)
    local vaoId = ffi.new("GLuint[1]", self.vao)
    gl.glDeleteVertexArrays(1, vaoId)
    gl.glBindVertexArray(0)
end

function cmdprompt:timestep(absTime, dt)
end

-- http://wiki.interfaceware.com/534.html
local function string_split(s, d)
    local t = {}
    local i = 0
    local f
    local match = '(.-)' .. d .. '()'
    
    if string.find(s, d) == nil then
        return {s}
    end
    
    for sub, j in string.gmatch(s, match) do
        i = i + 1
        t[i] = sub
        f = j
    end
    
    if i ~= 0 then
        t[i+1] = string.sub(s, f)
    end
    
    return t
end

-- Parse the args into a list of strings and reset the cmd line contents.
function cmdprompt:handleCommand()
    if not self.cmd_buf then return end

    -- Shell escape
    -- TODO This is dangerous!
    local ch1 = string.sub(self.cmd_buf,1,1)
    if ch1 == '!' then
        self.cmd_buf = string.sub(self.cmd_buf,2,#self.cmd_buf)
        print(self.cmd_buf)
        -- http://lua-users.org/lists/lua-l/2007-04/msg00085.html
        local f = io.popen(self.cmd_buf)
        local l = f:read("*a")
        print(l)
        f:close()
        self.cmd_buf = ""
        return
    end

    --print(self.cmd_buf)
    local args_with_empties = string_split(self.cmd_buf, ' ')
    local args = {}
    for k,v in pairs(args_with_empties) do
        if string.len(v) > 0 then
            table.insert(args,v)
        end
    end

    self.cmd_buf = ""
    return args
end

function cmdprompt:keypressed(key, scancode, action, mods)
    if not self.show then return end
    if action == 1 or action == 2 then
        if key == 259 then -- Backspace
            self.cmd_buf = string.sub(self.cmd_buf, 1, #self.cmd_buf-1)
        end

        return true
    end
end

function cmdprompt:charkeypressed(ch)
    if self.show then
        self.cmd_buf = self.cmd_buf..ch
        return
    end

    return true
end

function cmdprompt:onwheel(x,y)
    if self.ctrldown then
        local incr = 1.2
        if y > 0 then
            self.text_scale = self.text_scale * incr
        elseif y < 0 then
            self.text_scale = self.text_scale / incr
        end
        self.text_scale = math.min(self.text_scale, 2)
        self.text_scale = math.max(self.text_scale, .1)
        return true
    end
    return false
end

function cmdprompt:toggle()
    self.show = not self.show
end

function cmdprompt:renderPrompt(view, proj)
    -- Draw blended backdrop
    gl.glDisable(GL.GL_DEPTH_TEST)
    gl.glEnable(GL.GL_BLEND)
    gl.glUseProgram(self.prog_backdrop)
    gl.glBindVertexArray(self.vao)
    gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4)
    gl.glBindVertexArray(0)
    gl.glUseProgram(0)

    local col = {1, 1, 1}

    local m = {}
    mm.make_identity_matrix(m)
    mm.glh_translate(m, 60,45,0)
    local s = self.text_scale
    mm.glh_scale(m,s,s,s)
    local m2 = {}
    for i=1,16 do m2[i] = m[i] end

    local p = {}
    mm.glh_ortho(p, 0, self.win_w, self.win_h, 0, -1, 1)

    -- Command prompt
    local str = '$'..self.prompt_text..'>'..self.cmd_buf
    self.glfont:render_string(m2, p, col, str)
    mm.glh_translate(m2, 0,2*80,0)

    -- Output of last command
    for _,v in pairs(self.lines) do
        self.glfont:render_string(m2, p, col, v)
        mm.glh_translate(m2, 0,80,0)
    end
end

function cmdprompt:resizeViewport(w,h)
    self.win_w, self.win_h = w, h
end

return cmdprompt
