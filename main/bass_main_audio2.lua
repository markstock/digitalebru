-- main_audio.lua
-- generate a random waveform and play it using Bass.

local ffi = require("ffi")
package.path = package.path .. ';lib/?.lua'
local bass = require "bass"

-- Write to wav file
function writeWavFile(byte_array, numsampsc)
	local bytes_ptr = ffi.cast('unsigned char*', byte_array)
	local out = io.open('wavdata.wav', 'wb')
	for i=0,numsampsc*2 do
		out:write(string.char(bytes_ptr[i]))
	end
	out:close()

end

function playSoundForNSeconds(duration)
	-- Header
	local rate = 44100
	local numchan = 1
	local numsamps = duration * rate
	local numsampsc = numsamps * numchan
	local byte_array = ffi.new("int[?]", numsampsc)
	--[[
	struct wavfile_header {
		char	riff_tag[4];
		int	riff_length;
		char	wave_tag[4];
		char	fmt_tag[4];
		int	fmt_length;
		short	audio_format;
		short	num_channels;
		int	sample_rate;
		int	byte_rate;
		short	block_align;
		short	bits_per_sample;
		char	data_tag[4];
		int	data_length;
	};
	]]
	-- https://github.com/in4k/isystem1k4k/blob/master/i4k_OGLShader/src/main_win_deb.cpp
	byte_array[0] = 0x46464952
	byte_array[1] = numsampsc * 2 + 36
	byte_array[2] = 0x45564157
	byte_array[3] = 0x20746D66
	byte_array[4] = 16
	byte_array[5] = 1 + numchan*65536
	byte_array[6] = rate
	byte_array[7] = rate*numchan*2
	byte_array[8] = numchan*2 + (8*2)*0xFFFF
	byte_array[9] = 0x61746164
	byte_array[10] = numsampsc*2

	local tsLib = require("util.timeseries")
	ts = tsLib.new()
	ts:init("sin wave", 1)
	--ts:init("shakespeare play", 1)

	-- Sine wave
	local data_ptr = ffi.cast('unsigned short*', byte_array)
	local freq = 440
	local phase = 0
	local dt = 1/rate
	for i=0,numsampsc do
		local t = i/rate
		phase = phase + freq*dt
		local ampl = 0.25*0xFFFF*ts:getval(math.fmod(phase,1))
		data_ptr[i+(11*2)] = ampl
	end

	writeWavFile(byte_array, numsampsc)

	stream = bass.BASS_StreamCreateFile(1, byte_array, 0, numsampsc, 0)

	bass.BASS_Start()
	r = bass.BASS_ChannelPlay(stream, true)
	print(r)
end

function main()
	local init_ret = bass.BASS_Init(-1, 44100, 2, 0, nil)
	print("Bass init:", init_ret)
	local duration = .5
	playSoundForNSeconds(duration)
	duration = 2

	-- delay
	local clock = os.clock
	local start = clock()

	-- Just sleep until the end
	while clock() - start < duration do end

	bass.BASS_Stop()
	bass.BASS_Free()
end

main()
