-- glfw_shader2.lua
-- Aiming for minimal bytecode. Export with:
-- ./bin/linux/luajit -b main/glfw_shader2.lua glfwsh.out

local glfw = require('lib.glfw')
local ffi = require('ffi')

local glheader = [[
typedef void GLvoid;
typedef unsigned int GLenum;
typedef int GLint;
typedef int GLsizei;
typedef unsigned int GLuint;
typedef char GLchar;
typedef void (APIENTRYP PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint (APIENTRYP PFNGLCREATEPROGRAMPROC) (void);
typedef GLuint (APIENTRYP PFNGLCREATESHADERPROC) (GLenum type);
typedef void (APIENTRYP PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (APIENTRYP PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
typedef void (APIENTRYP PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (APIENTRYP PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (APIENTRYP PFNGLDRAWARRAYSPROC) (GLenum mode, GLint first, GLsizei count);
]]

if ffi.os == "Windows" then
    glheader = glheader:gsub("APIENTRYP", "__stdcall *")
    glheader = glheader:gsub("APIENTRY", "__stdcall")
else
    glheader = glheader:gsub("APIENTRYP", "*")
    glheader = glheader:gsub("APIENTRY", "")
end

ffi.cdef(glheader)

local vertsrc = [[
#version 300 es
out vec3 vfColor;

void main()
{
    float v[6] = float[](-3.,-1.,1.,-1.,1.,3.);
    int i = gl_VertexID;
    vec2 vPos = vec2(v[2*i],v[2*i+1]);
    vfColor = vec3(vPos, 0.);
    gl_Position = vec4(vPos, 0., 1.);
}
]]

local fragsrc = [[
#version 300 es
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

in vec3 vfColor;
out vec4 fragColor;

void main()
{
    vec3 col = .5*vfColor + vec3(.5);
    fragColor = vec4(col, 1.0);
}
]]

glfw.glfw.Init()
window = glfw.glfw.CreateWindow(800,600,"GL",nil,nil)
glfw.glfw.MakeContextCurrent(window)

function getFunction(name)
    local loader = glfw.glfw.GetProcAddress
    local glname = name
    local procname = "PFN" .. name:upper() .. "PROC"
    return ffi.cast(procname, loader(glname))
end

function shader(src, type)
    local shaderObject = getFunction('glCreateShader')(type)
    local len = ffi.new('int[1]')
    len[0] = #src
    local strs = ffi.new('const char*[1]')
    strs[0] = src
    getFunction('glShaderSource')(shaderObject, 1, strs, len)
    getFunction('glCompileShader')(shaderObject)
    return shaderObject
end

local program = getFunction('glCreateProgram')()
getFunction('glAttachShader')(program, shader(vertsrc, 0x8B31))--GL.GL_VERTEX_SHADER
getFunction('glAttachShader')(program, shader(fragsrc, 0x8B30))--GL.GL_FRAGMENT_SHADER
getFunction('glLinkProgram')(program)
getFunction('glUseProgram')(program)

while glfw.glfw.WindowShouldClose(window) == 0 do
    if glfw.glfw.GetKey(window, 256) == 1 then return end -- glfw.GLFW.KEY_ESCAPE
    glfw.glfw.PollEvents()
    getFunction('glDrawArrays')(0x0006, 0, 3) --GL_TRIANGLE_FAN
    glfw.glfw.SwapBuffers(window)
end
