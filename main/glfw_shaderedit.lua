--[[ main_shaderedit.lua

    Something like a desktop version of shadertoy.
]]
print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'
local ffi = require "ffi"
local glfw = require "glfw"
local openGL = require "opengl"
openGL.loader = glfw.glfw.GetProcAddress
openGL:import()
local DEBUG = false
local DEBUG_quitonerror = false

local snd = require "util.soundfx"

local socket = nil
if (ffi.os == "Windows") then
    package.cpath = package.cpath .. ';bin/windows/socket/core.dll'
    package.loadlib("socket/core.dll", "luaopen_socket_core")
    socket = require 'socket.core'
elseif (ffi.os == "Linux") then
    package.cpath = package.cpath .. ';bin/linux/socket/?.so'
    socket = require 'socket.core'
elseif (ffi.os == "OSX") then
    package.cpath = package.cpath .. ';bin/osx/socket/?.so'
    socket = require 'socket.core'
end

local clock = os.clock
if socket then
    print("Using socket.gettime in Luasocket version "..socket._VERSION)
    clock = socket.gettime
end

require "util.glfont"
local CameraLibrary = require "util.camera"
local EditorLibrary = require "scene.stringedit_scene"
local mm = require "util.matrixmath"
local fpstimer = require "util.fpstimer"

local Scene = nil
local glfont = nil
local Camera = CameraLibrary.new()
local Editor = nil

local win_w = 2160/2
local win_h = 1440/2
local fb_w = win_w
local fb_h = win_h
local g_ft = fpstimer.create()
local lastSceneChangeTime = 0

local editor_toggle_first_char_hack = nil
local caught_error_in_draw = nil

local scenedir = "scene"

function switch_to_scene(name)
    local fullname = scenedir.."."..name
    if Scene and Scene.exitGL then
        Scene:exitGL()
    end
    -- Do we need to unload the module?
    package.loaded[fullname] = nil
    Scene = nil

    if not Scene then
        local SceneLibrary = require(fullname)
        Scene = SceneLibrary.new()
        if Scene then
            local now = clock()
            if Scene.setDataDirectory then Scene:setDataDirectory("data") end
            Scene:initGL()
            local initTime = clock() - now
            lastSceneChangeTime = now
            collectgarbage()
            print(name,
                "init time: "..math.floor(1000*initTime).." ms",
                "memory: "..math.floor(collectgarbage("count")).." kB")
        end
    end
end

local scene_modules = {
    "shadertoy",
    "vsfstri",
}

local scene_module_idx = 1
function switch_scene(reverse)
    if reverse then
        scene_module_idx = scene_module_idx - 1
        if scene_module_idx < 1 then scene_module_idx = #scene_modules end
    else
        scene_module_idx = scene_module_idx + 1
        if scene_module_idx > #scene_modules then scene_module_idx = 1 end
    end
    switch_to_scene(scene_modules[scene_module_idx])

    -- Sound from http://rcptones.com/dev_tones/#tabr1
    snd.playSound("pop_drip.wav")
end

function toggle_editor()
    if Editor == nil then
        local filename = Scene.data_dir ..'/'.. "fragshader.glsl"
        Editor = EditorLibrary.new(filename)
        if Editor then
            if Editor.setDataDirectory then Editor:setDataDirectory("data") end
            Editor:initGL()
            editor_toggle_first_char_hack = true
        end    
    else
        Editor:exitGL()
        Editor = nil
    end
end

-- http://lua-users.org/wiki/SplitJoin
function split_into_lines(str)
    local t = {}
    local function helper(line) table.insert(t, line) return "" end
    helper((str:gsub("(.-)\r?\n", helper)))
    return t
end

function push_error_messages_into_editor(err)
    -- Catch, parse, and display this error
    -- overlaid on the editor's code
    print(err)
    print(debug.traceback())
    -- Grab the last line of err msg
    local lines = split_into_lines(err)
    if #lines == 0 then return end
    local linemsg = lines[#lines]
    -- Extract line number between colons
    local a = string.find(linemsg, ':')
    local b = string.find(linemsg, ':', a+1)
    if not b then return end
    local errtext = string.sub(linemsg,b+2,#linemsg)
    local linenum = string.sub(linemsg, a+1, b-1)
    Editor.error_msgs[tonumber(linenum)] = errtext
end

function save_and_reload_scene()
    caught_error_in_draw = nil
    local status, err = pcall(function ()
        -- Reload scene, catching any errors
        -- TODO: why won't the next call work?
        --switch_to_scene(scene_modules[scene_module_idx])
        switch_scene(true)
        switch_scene(false)
    end)

    -- Clear out error messages in editor
    for k in pairs (Editor.error_msgs) do
        Editor.error_msgs[k] = nil
    end

    if not status then
        push_error_messages_into_editor(err)
    end
end

function onkey(window, key, scancode, action, mods)
    local altdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_ALT)
    local ctrldown = 0 ~= bit.band(mods, glfw.GLFW.MOD_CONTROL)
    local shiftdown = 0 ~= bit.band(mods, glfw.GLFW.MOD_SHIFT)

    -- If editor active, pass keys to it
    if Editor then
        if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
            -- Check for our escape sequence
            if key == glfw.GLFW.KEY_GRAVE_ACCENT or
                key == glfw.GLFW.KEY_ESCAPE then
                toggle_editor()
            else
                Editor:keypressed(key, scancode, action, mods)

                -- On save, reload the scene
                if mods == 2 then -- ctrl held
                    if action == 1 then -- press
                        if key == string.byte('S') then
                            save_and_reload_scene()
                        end
                    end
                end
            end
            return
        end
    end

    -- Then try main's key handler
    local handled = false
    local func_table_ctrl = {
        [glfw.GLFW.KEY_GRAVE_ACCENT] = function (x) switch_scene(shiftdown) end,
        [glfw.GLFW.KEY_TAB] = function (x) switch_scene(shiftdown) end,
    }
    local func_table = {
        [glfw.GLFW.KEY_ESCAPE] = function (x) glfw.glfw.SetWindowShouldClose(window, 1) end,
        [glfw.GLFW.KEY_R] = function (x) Camera:reset() end,
        [glfw.GLFW.KEY_GRAVE_ACCENT] = function (x) toggle_editor() end,
    }
    if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
        if ctrldown then
            local f = func_table_ctrl[key]
            if f then f() handled = true end
        else
            local f = func_table[key]
            if f then f() handled = true end
        end
    end

    -- Now pass to Scene's handler
    if handled == false then
        if action == glfw.GLFW.PRESS or action == glfw.GLFW.REPEAT then
            if Scene.keypressed then
                local consumed = Scene:keypressed(key, scancode, action, mods)
                if consumed then return end
            end
        end
    end

    Camera:onkey(key, scancode, action, mods)
end

-- Passing raw character values to scene lets us get away with not
-- including glfw key enums in scenes.
function onchar(window,ch)
    if Editor then

        -- Glfw calls onchar immediately after keypressed, so the tilde
        -- press to toggle the editor issues a char to the file on every
        -- open. This is the workaround.
        if editor_toggle_first_char_hack then
            editor_toggle_first_char_hack = nil
            return
        end

        Editor:charkeypressed(string.char(ch))
        return
    end

    if Scene.charkeypressed then
        Scene:charkeypressed(string.char(ch))
    end
end

function onclick(window, button, action, mods)
    local double_buffer = ffi.new("double[2]")
    glfw.glfw.GetCursorPos(window, double_buffer, double_buffer+1)
    local x,y = double_buffer[0], double_buffer[1]
    Camera:onclick(button, action, mods, x, y)
end

function onmousemove(window, x, y)
    if Editor then
        -- TODO: pass it through? Mouse handling for editor?
        return
    end

    Camera:onmousemove(x,y)
    if Camera.holding == glfw.GLFW.MOUSE_BUTTON_1 then
        if Scene.onmouse ~= nil then
            Scene:onmouse(x/win_w, y/win_h)
        end
    end
end

function onwheel(window,x,y)
    if Editor then
        Editor:onwheel(x,y)
        return
    end

    Camera:onwheel(x,y)
end

function resize(window, w, h)
    win_w, win_h = w, h
    fb_w, fb_h = w,h
end

function initGL()
    local status, err = pcall(function ()
        switch_to_scene(scene_modules[scene_module_idx])
    end)
    if not status then
        print(err)
    end

    local fontname = 'segoe_ui128'
    glfont = GLFont.new(fontname..'.fnt', fontname..'_0.raw')
    glfont:setDataDirectory("data/fonts")
    glfont:initGL()

    snd.setDataDirectory("data")
end

function exitGL()
    if Scene and Scene.exitGL then Scene:exitGL() end
    glfont:exitGL()
end

function display_scene_overlay()
    if not glfont then return end

    local showTime = 2
    local age = clock() - lastSceneChangeTime
    -- TODO a nice fade or something
    if age > showTime then return end

    local m = {}
    local p = {}
    mm.make_identity_matrix(m)
    local s = .5
    mm.glh_scale(m, s, s, s)

    -- Draw scene name sliding down from window top
    -- Appearing on scene change, disappearing shortly.
    local yoff = 0
    local tin = .15
    local tout = .5
    local yslide = -250
    if age < tin then yoff = yslide * (1-age/tin) end
    if age > showTime - tout then yoff = yslide * (age-(showTime-tout)) end
    mm.glh_translate(m, 30, yoff, 0)
    -- TODO getStringWidth and center text
    mm.glh_ortho(p, 0, win_w, win_h, 0, -1, 1)
    gl.glDisable(GL.GL_DEPTH_TEST)
    local col = {1, 1, 1}
    local name = scene_module_idx .. ") " .. scene_modules[scene_module_idx]
    glfont:render_string(m, p, col, name)
end

function display()
    gl.glClearColor(0.5, 0.5, 1.0, 0.0)
    gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)
    gl.glEnable(GL.GL_DEPTH_TEST)

    if Scene then
        local v = Camera:getmatrix()
        mm.affine_inverse(v)
        local p = {}
        local aspect = win_w / win_h
        mm.glh_perspective_rh(p, 90, aspect, .004, 500)

        local status, err = pcall(function ()
            Scene:render_for_one_eye(v,p)
        end)
        if not status then
            -- TODO: catch and print error only once here
            if not caught_error_in_draw then
                caught_error_in_draw = true
                print(err)
                push_error_messages_into_editor(err)
            end
        end

        if Scene.set_origin_matrix then Scene:set_origin_matrix(v) end
    end
end

function display_with_effect()
    gl.glViewport(0,0, fb_w, fb_h)
    display()

    if Editor then
        local id = {}
        mm.make_identity_matrix(id)
        local ids = {}
        local aspect = win_w / win_h
        mm.make_scale_matrix(ids,1/aspect,1,1)
        local s = .9
        mm.glh_scale(ids,s,s,s)
        mm.glh_translate(ids,.3,0,0)

        local v = Camera:getmatrix()
        mm.affine_inverse(v)
        local p = {}
        mm.glh_perspective_rh(p, 90, aspect, .004, 500)
        gl.glDisable(GL.GL_DEPTH_TEST)
        Editor:render_for_one_eye(ids,id)
        gl.glEnable(GL.GL_DEPTH_TEST)
    end

    display_scene_overlay()
end

function timestep(absTime, dt)
    if Scene and Scene.timestep then Scene:timestep(absTime, dt) end
    Camera:timestep(absTime, dt)
end

ffi.cdef[[
typedef unsigned int GLenum;
typedef unsigned int GLuint;
typedef int GLsizei;
typedef char GLchar;
///@todo APIENTRY
typedef void (__stdcall *GLDEBUGPROC)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
]]

local count = 0
local myCallback = ffi.cast("GLDEBUGPROC", function(source, type, id, severity, length, message, userParam)
    if severity == GL.GL_DEBUG_SEVERITY_NOTIFICATION then return end
    local enum_table = {
        [tonumber(GL.GL_DEBUG_SEVERITY_HIGH)] = "SEVERITY_HIGH",
        [tonumber(GL.GL_DEBUG_SEVERITY_MEDIUM)] = "SEVERITY_MEDIUM",
        [tonumber(GL.GL_DEBUG_SEVERITY_LOW)] = "SEVERITY_LOW",
        [tonumber(GL.GL_DEBUG_SEVERITY_NOTIFICATION)] = "SEVERITY_NOTIFICATION",
        [tonumber(GL.GL_DEBUG_SOURCE_API)] = "SOURCE_API",
        [tonumber(GL.GL_DEBUG_SOURCE_WINDOW_SYSTEM)] = "SOURCE_WINDOW_SYSTEM",
        [tonumber(GL.GL_DEBUG_SOURCE_SHADER_COMPILER)] = "SOURCE_SHADER_COMPILER",
        [tonumber(GL.GL_DEBUG_SOURCE_THIRD_PARTY)] = "SOURCE_THIRD_PARTY",
        [tonumber(GL.GL_DEBUG_SOURCE_APPLICATION)] = "SOURCE_APPLICATION",
        [tonumber(GL.GL_DEBUG_SOURCE_OTHER)] = "SOURCE_OTHER",
        [tonumber(GL.GL_DEBUG_TYPE_ERROR)] = "TYPE_ERROR",
        [tonumber(GL.GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR)] = "TYPE_DEPRECATED_BEHAVIOR",
        [tonumber(GL.GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR)] = "TYPE_UNDEFINED_BEHAVIOR",
        [tonumber(GL.GL_DEBUG_TYPE_PORTABILITY)] = "TYPE_PORTABILITY",
        [tonumber(GL.GL_DEBUG_TYPE_PERFORMANCE)] = "TYPE_PERFORMANCE",
        [tonumber(GL.GL_DEBUG_TYPE_MARKER)] = "TYPE_MARKER",
        [tonumber(GL.GL_DEBUG_TYPE_PUSH_GROUP)] = "TYPE_PUSH_GROUP",
        [tonumber(GL.GL_DEBUG_TYPE_POP_GROUP)] = "TYPE_POP_GROUP",
        [tonumber(GL.GL_DEBUG_TYPE_OTHER)] = "TYPE_OTHER",
    }
    print(enum_table[source], enum_table[type], enum_table[severity], id)
    print("   "..ffi.string(message))
    print("   Stack Traceback\n   ===============")
    -- Chop off the first lines of the traceback, as it's always this function.
    local tb = debug.traceback()
    local i = string.find(tb, '\n')
    local i = string.find(tb, '\n', i+1)
    print(string.sub(tb,i+1,-1))
    if DEBUG_quitonerror then exit(1) end
end)

function main()
    for k,v in pairs(arg) do
        if k > 0 then
            if v == '-d' then DEBUG = true end
            if v == '-D' then
                DEBUG = true
                DEBUG_quitonerror = true
            end
        end
    end

    glfw.glfw.Init()

    if ffi.os == "OSX" then
        glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MINOR, 1) -- 3 causes MacOSX to segfault
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_PROFILE, glfw.GLFW.OPENGL_CORE_PROFILE)
    else
        glfw.glfw.WindowHint(glfw.GLFW.DEPTH_BITS, 16)
        glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MINOR, 3)
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_PROFILE, glfw.GLFW.OPENGL_ANY_PROFILE)
    end
    glfw.glfw.WindowHint(glfw.GLFW.CONTEXT_VERSION_MAJOR, 4)
    glfw.glfw.WindowHint(glfw.GLFW.OPENGL_FORWARD_COMPAT, 1)
    --glfw.glfw.WindowHint(glfw.GLFW.CLIENT_API, glfw.GLFW.OPENGL_ES_API)

    if DEBUG then
        glfw.glfw.WindowHint(glfw.GLFW.OPENGL_DEBUG_CONTEXT, GL.GL_TRUE)
    end

    window = glfw.glfw.CreateWindow(win_w,win_h,"Luajit",nil,nil)
    local int_buffer = ffi.new("int[2]")
    -- Get actual framebuffer size for oversampled ("Retina") displays
    glfw.glfw.GetFramebufferSize(window, int_buffer, int_buffer+1)
    fb_w = int_buffer[0]
    fb_h = int_buffer[1]
    glfw.glfw.SetKeyCallback(window, onkey)
    glfw.glfw.SetCharCallback(window, onchar);
    glfw.glfw.SetMouseButtonCallback(window, onclick)
    glfw.glfw.SetCursorPosCallback(window, onmousemove)
    glfw.glfw.SetScrollCallback(window, onwheel)
    glfw.glfw.SetWindowSizeCallback(window, resize)
    glfw.glfw.MakeContextCurrent(window)
    glfw.glfw.SwapInterval(0)
    if DEBUG then
        gl.glDebugMessageCallback(myCallback, nil)
        gl.glDebugMessageControl(GL.GL_DONT_CARE, GL.GL_DONT_CARE, GL.GL_DONT_CARE, 0, nil, GL.GL_TRUE)
        gl.glDebugMessageInsert(GL.GL_DEBUG_SOURCE_APPLICATION, GL.GL_DEBUG_TYPE_MARKER, 0,
            GL.GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging")
        gl.glEnable(GL.GL_DEBUG_OUTPUT_SYNCHRONOUS);
    end

    local windowTitle = "OpenGL with Luajit"
    local firstFrameTime = clock()
    local lastFrameTime = clock()
    initGL()
    while glfw.glfw.WindowShouldClose(window) == 0 do
        glfw.glfw.PollEvents()
        g_ft:onFrame()
        display_with_effect()

        local now = clock()
        timestep(now - firstFrameTime, now - lastFrameTime)
        lastFrameTime = now

        if ffi.os == "Windows" or ffi.os == "OSX" then
            glfw.glfw.SetWindowTitle(window, windowTitle.." "..math.floor(g_ft:getFPS()).." fps")
        end

        glfw.glfw.SwapBuffers(window)
    end
    exitGL()
end

main()
