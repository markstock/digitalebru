-- sdl_modifiers.lua
-- A test of modifier handling in SDL
print(jit.version, jit.os, jit.arch)

package.path = package.path .. ';lib/?.lua'
local ffi = require 'ffi'
local SDL = require 'lib/sdl'
local openGL = require 'opengl'
openGL.loader = SDL.SDL_GL_GetProcAddress
openGL:import()

function main()
    local window
    local win_w = 800
    local win_h = 600

    SDL.SDL_Init(SDL.SDL_INIT_VIDEO)

    window = SDL.SDL_CreateWindow(
        "An SDL2 window",
        SDL.SDL_WINDOWPOS_UNDEFINED,
        SDL.SDL_WINDOWPOS_UNDEFINED,
        win_w,
        win_h,
        ffi.C.SDL_WINDOW_OPENGL +
        ffi.C.SDL_WINDOW_SHOWN +
        ffi.C.SDL_WINDOW_RESIZABLE
    )

    if window == nil then
        print("Could not create window: " .. ffi.string( SDL.SDL_GetError() ))
        os.exit(1)
    end

    glContext = SDL.SDL_GL_CreateContext(window);
    if glContext == nil then
        print("There was an error creating the OpenGL context!\n")
        return 0
    end

    SDL.SDL_GL_MakeCurrent(window, glContext)

    local quit = false
    while not quit do
        local event = ffi.new("SDL_Event")
        while SDL.SDL_PollEvent(event) ~= 0 do
            if event.type == SDL.SDL_KEYDOWN then
                if event.key.keysym.sym == SDL.SDLK_ESCAPE then
                    quit = true
                end
            end
        end

        gl.glViewport(0,0, win_w, win_h)
        local Keymod = SDL.SDL_GetModState()
        gl.glClearColor(
            0 ~= bit.band(tonumber(Keymod), SDL.KMOD_LALT) and 1 or 0,
            0 ~= bit.band(tonumber(Keymod), SDL.KMOD_LCTRL) and 1 or 0,
            0 ~= bit.band(tonumber(Keymod), SDL.KMOD_LSHIFT) and 1 or 0,
            0)
        gl.glClear(GL.GL_COLOR_BUFFER_BIT + GL.GL_DEPTH_BUFFER_BIT)

        SDL.SDL_GL_SwapWindow(window)
    end

    SDL.SDL_DestroyWindow(window)
    SDL.SDL_Quit()
end

main()
